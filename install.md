# Distribution CentOS

### Robin Peignet

---

## Sommaire

* Installation du CentOS
* Installation de ssh
* Installation et configuration de git

---

## CentOS & Apache

### Descriptif

CentOS est une distribution linux éditée par la société Red Hat. Elle est destinée à une utilisation serveur (sans interface graphique) notament dans le cadre d'un serveur web (environ 20% du marché).
CentOS a connu plusieurs dérives comme RockyLinux ou Centreon.

---

### Installation

[Lien de téléchargement CentOS](https://www.centos.org/download/).

On suit l'installateur Red Hat en configurant les options d'installation :
* Fuseau horaire
* Mot de passe admin
* ...

On définit également un utilisateur 'rpeignet' (qui ne possède pas les droits admin) et qui sera l'utilisateur principal du système.

---

Pour tester l'installation CentOS serveur, on installe le serveur web Apache (dans un cadre de serveur de développement par exemple).

```
> sudo dnf install httpd
```

On lance le serveur qui ne se lance pas automatiquement et on accorde les droits sur le dossier '/var/www/html' pour l'utilisateur 'rpeignet'.
Enfin on lance un navigateur pour tester le serveur sur l'adresse ip de notre machine virtuelle (attribuer par DHCP) :

![image](images/apache.png)

---

## Installation de ssh

On installe ssh sur notre système CentOS pour pouvoir nous connecter au serveur host à partir d'une machine distante. 


```
> sudo dnf install openssh-server
```

On lance ssh puis on vérifie qu'il est bien activé :
```
> sudo systemctl start sshd

> sudo systemctl status sshd
```

![image](images/ssh.png)


Dans notre cas, on utilisera l'éditeur Visual Studio Code (avec l'extension Remote Explorer) pour se connecter au serveur. On ajoute une nouvelle instance de connection et on se connecte avec l'utilisateur 'rpeignet' créé auparavant :
```
> ssh rpeignet@192.168.1.9 -A
```

---

## Installation et configuration de git

On installe Git sur notre système CentOS pour pouvoir versionner les fichiers modifiés dans le répertoire "home/rpeignet/rapport".

Installation :
```
> sudo dnf install git
```

On vérifie que l'installation a fonctionné :
```
> git --version
```
![image](images/git.png)

Une fois nos modifications terminées, on les pousse sur le [repository git admin_linux](https://gitlab.com/112002robin/admin_linux).